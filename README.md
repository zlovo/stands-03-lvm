# Выполнение ДЗ урок 3 LVM

* Клонируем Git
* Запускаем Виртуальную Машину `vagrant up` заходим `vagrant ssh`
* Смотрим что на диске 

```
[root@lvm ~]# lsblk
NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                       8:0    0   40G  0 disk 
├─sda1                    8:1    0    1M  0 part 
├─sda2                    8:2    0    1G  0 part /boot
└─sda3                    8:3    0   39G  0 part 
  ├─VolGroup00-LogVol00 253:0    0 37.5G  0 lvm  /
  └─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
sdb                       8:16   0   10G  0 disk 
sdc                       8:32   0    2G  0 disk 
sdd                       8:48   0    1G  0 disk 
sde                       8:64   0    1G  0 disk 
```
а также 
```
[root@lvm ~]# lvmdiskscan
  /dev/VolGroup00/LogVol00 [     <37.47 GiB] 
  /dev/VolGroup00/LogVol01 [       1.50 GiB] 
  /dev/sda2                [       1.00 GiB] 
  /dev/sda3                [     <39.00 GiB] LVM physical volume
  /dev/sdb                 [      10.00 GiB] 
  /dev/sdc                 [       2.00 GiB] 
  /dev/sdd                 [       1.00 GiB] 
  /dev/sde                 [       1.00 GiB] 
  4 disks
  3 partitions
  0 LVM physical volume whole disks
  1 LVM physical volume
```
Созадим физичсекий том PV на блочном усторойстве /dev/sdb для будущего использования LVM:
```
[root@lvm ~]# pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
```
Создадим группу томов VG - первый уровень абстракции

```
root@lvm ~]# vgcreate otus /dev/sdb
  Volume group "otus" successfully created
```
Теперь создаем логический том  Logical Volume (далее - LV):
```
[root@lvm ~]# lvcreate -l+80%FREE -n test otus
  Logical volume "test" created.
```
Смотрим инвормацию о созданном томе 
```
[root@lvm ~]# vgdisplay otus
  --- Volume group ---
  VG Name               otus
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <10.00 GiB
  PE Size               4.00 MiB
  Total PE              2559
  Alloc PE / Size       2047 / <8.00 GiB
  Free  PE / Size       512 / 2.00 GiB
  VG UUID               wvgDe1-6RrR-qERt-YPEi-Wdne-8Qcl-y8teQ6
```
еше посмотрим при помощи `lsblk`

```
[root@lvm ~]# lsblk
NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                       8:0    0   40G  0 disk 
├─sda1                    8:1    0    1M  0 part 
├─sda2                    8:2    0    1G  0 part /boot
└─sda3                    8:3    0   39G  0 part 
  ├─VolGroup00-LogVol00 253:0    0 37.5G  0 lvm  /
  └─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
sdb                       8:16   0   10G  0 disk 
└─otus-test             253:2    0    8G  0 lvm  
sdc                       8:32   0    2G  0 disk 
sdd                       8:48   0    1G  0 disk 
sde                       8:64   0    1G  0 disk 
```
Еще посмотим информацию о том, какие диски входит в VG

```
vgdisplay -v otus | grep PV
  Max PV                0
  Cur PV                1
  Act PV                1
  PV Name               /dev/sdb     
  PV UUID               qm2kp9-cLSS-Ai9U-IjMi-e2VG-QUPx-P7wOpv
  PV Status             allocatable

```

Посмотрим детальную информацию о логическом томе

```
[root@lvm ~]# lvdisplay /dev/otus/test
  --- Logical volume ---
  LV Path                /dev/otus/test
  LV Name                test
  VG Name                otus
  LV UUID                qoVsT1-FD1K-oJdy-GSfF-qXe8-UGPj-zpQNs5
  LV Write Access        read/write
  LV Creation host, time lvm, 2020-05-29 16:27:27 +0000
  LV Status              available
  # open                 0
  LV Size                <8.00 GiB
  Current LE             2047
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
```

Получим краткую информацию командами `vgs` и `lvs`

```
[root@lvm ~]# vgs
  VG         #PV #LV #SN Attr   VSize   VFree
  VolGroup00   1   2   0 wz--n- <38.97g    0 
  otus         1   1   0 wz--n- <10.00g 2.00g
[root@lvm ~]# lvs
  LV       VG         Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  LogVol00 VolGroup00 -wi-ao---- <37.47g                                                    
  LogVol01 VolGroup00 -wi-ao----   1.50g                                                    
  test     otus       -wi-a-----  <8.00g  
```

Создадим на LV файловую систему 

```
[root@lvm ~]# mkfs.ext4 /dev/otus/test
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
524288 inodes, 2096128 blocks
104806 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=2147483648
64 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done 
```

Смонтируем его к директории

```
[root@lvm ~]# mkdir /data
[root@lvm ~]# mount /dev/otus/test /data/
[root@lvm ~]# mount | grep /data
/dev/mapper/otus-test on /data type ext4 (rw,relatime,seclabel,data=ordered)
```
## LVM Resizing

### Расширение LVM
Допустим перед нами встала проблема нехватки свободного места в директории `/data`. 
Мы можем расширитþ файловую систему на `LV /dev/otus/test` за счет нового блочного устройства `/dev/sdc`.

Создаем физический том PV:

```
[root@lvm ~]# pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
```

Расширим группу VG, добавив в нее этот диск (блочное устройство)

```
[root@lvm ~]# vgextend otus /dev/sdc
  Volume group "otus" successfully extended
```

Проверим, что новый диск присутсвет в расширенной группе VG

```
[root@lvm ~]# vgdisplay -v otus | grep 'PV Name'
  PV Name               /dev/sdb     
  PV Name               /dev/sdc     
```

Проверим, что места в VG добавилось

```
[root@lvm ~]# vgs
  VG         #PV #LV #SN Attr   VSize   VFree 
  VolGroup00   1   2   0 wz--n- <38.97g     0 
  otus         2   1   0 wz--n-  11.99g <4.00g
```

Используем комманду dd для заполнения места на VG 

```
[root@lvm ~]# dd if=/dev/zero of=/data/test.log bs=1M count=8000 status=progress
8210350080 bytes (8.2 GB) copied, 131.918733 s, 62.2 MB/s
dd: error writing ‘/data/test.log’: No space left on device
7880+0 records in
7879+0 records out
8262189056 bytes (8.3 GB) copied, 132.888 s, 62.2 MB/s
```

Проверим, что у нас занято 100% дискового пространства 

```
[root@lvm ~]# df -Th /data/
Filesystem            Type  Size  Used Avail Use% Mounted on
/dev/mapper/otus-test ext4  7.8G  7.8G     0 100% /data
```

Увеличиваем LV за счет появивщегося свободного места. При этом возмём не все место а только 80% свободного места

```
[root@lvm ~]# lvextend -l+80%FREE /dev/otus/test
  Size of logical volume otus/test changed from <8.00 GiB (2047 extents) to <11.20 GiB (2866 extents).
  Logical volume otus/test successfully resized.
```

Наблюдаем на сколько увеличился LV - ответ: до 11.20g:

```
[root@lvm ~]# lvs /dev/otus/test
  LV   VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  test otus -wi-ao---- <11.20g  
```
Но файловая система при этом осталась прежнего размера 

```
[root@lvm ~]# df -Th /data
Filesystem            Type  Size  Used Avail Use% Mounted on
/dev/mapper/otus-test ext4  7.8G  7.8G     0 100% /data
```

Произведем resize файловой системы 

```
[root@lvm ~]# resize2fs /dev/otus/test
resize2fs 1.42.9 (28-Dec-2013)
Filesystem at /dev/otus/test is mounted on /data; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 2
The filesystem on /dev/otus/test is now 2934784 blocks long.
```

Посмотрим теперь на размер файловой системы и увидим увеличение 

```
[root@lvm ~]# df -Th /data
Filesystem            Type  Size  Used Avail Use% Mounted on
/dev/mapper/otus-test ext4   11G  7.8G  2.7G  75% /data
```
### Уменьшение LVM

Допустим мы забыли оставить место на снэшоты. Можно уменьшить уже существующие LV с
помощью команды lvreduce, 

но перед этим необходимо отмонтировать файловую систему:
```
[root@lvm ~]# umount /data/
```
проверить ее на ошибки:
```
[root@lvm ~]# e2fsck -fy /dev/otus/test
e2fsck 1.42.9 (28-Dec-2013)
Pass 1: Checking inodes, blocks, and sizes
Pass 2: Checking directory structure
Pass 3: Checking directory connectivity
Pass 4: Checking reference counts
Pass 5: Checking group summary information
/dev/otus/test: 12/737280 files (0.0% non-contiguous), 2106421/2934784 blocks
```
изменить размер фафловой системы:
```
[root@lvm ~]# resize2fs /dev/otus/test 10G
resize2fs 1.42.9 (28-Dec-2013)
Resizing the filesystem on /dev/otus/test to 2621440 (4k) blocks.
The filesystem on /dev/otus/test is now 2621440 blocks long.
```
Собственно уменьщаем размер существующего LV с
помощью команды lvreduce:
```
[root@lvm ~]# lvreduce /dev/otus/test -L 10G
  WARNING: Reducing active logical volume to 10.00 GiB.
  THIS MAY DESTROY YOUR DATA (filesystem etc.)
Do you really want to reduce otus/test? [y/n]: y
  Size of logical volume otus/test changed from <11.20 GiB (2866 extents) to 10.00 GiB (2560 extents).
  Logical volume otus/test successfully resized.
```
и монтируем обратно 
```
[root@lvm ~]# mount /dev/otus/test /data/
```

Убедимся, что ФС и lvm необходимого размера:
```
[root@lvm ~]# df -Th /data/
Filesystem            Type  Size  Used Avail Use% Mounted on
/dev/mapper/otus-test ext4  9.8G  7.8G  1.6G  84% /data
```
и 
```
[root@lvm ~]# lvs /dev/otus/test
  LV   VG   Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  test otus -wi-ao---- 10.00g
```
## LVM Snapshot
Снапшот создается командой lvcreate, только с флагом -s, который указывает на то, что это снимок:
```
[root@lvm ~]# lvcreate -L 500M -s -n test-snap /dev/otus/test
  Logical volume "test-snap" created.
```
Проверим с помощью команды `vgs` :
```
[root@lvm ~]# sudo vgs -o +lv_size,lv_name | grep test
  otus         2   3   1 wz--n-  11.99g <1.41g  10.00g test     
  otus         2   3   1 wz--n-  11.99g <1.41g 500.00m test-snap
```
Посмотрим, что произошло при помощи комманды `lsbk`, где помимо оригинала `otus-test` появился снэпшот `otus-test--snap`
а в copy on write COW будут писаться изменения 
```
[root@lvm ~]# lsblk
NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                       8:0    0   40G  0 disk 
├─sda1                    8:1    0    1M  0 part 
├─sda2                    8:2    0    1G  0 part /boot
└─sda3                    8:3    0   39G  0 part 
  ├─VolGroup00-LogVol00 253:0    0 37.5G  0 lvm  /
  └─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
sdb                       8:16   0   10G  0 disk 
├─otus-small            253:3    0  100M  0 lvm  
└─otus-test-real        253:4    0   10G  0 lvm  
  ├─otus-test           253:2    0   10G  0 lvm  /data
  └─otus-test--snap     253:6    0   10G  0 lvm  
sdc                       8:32   0    2G  0 disk 
├─otus-test-real        253:4    0   10G  0 lvm  
│ ├─otus-test           253:2    0   10G  0 lvm  /data
│ └─otus-test--snap     253:6    0   10G  0 lvm  
└─otus-test--snap-cow   253:5    0  500M  0 lvm  
  └─otus-test--snap     253:6    0   10G  0 lvm  
sdd                       8:48   0    1G  0 disk 
sde                       8:64   0    1G  0 disk 
```
Монтируем снэпшот и смотрим что там, отмонтируем
```
[root@lvm ~]# mkdir /data-snap
[root@lvm ~]# mount /dev/otus/test-snap /data-snap/
[root@lvm ~]# ll /data-snap/
total 8068564
drwx------. 2 root root      16384 Jun  6 00:15 lost+found
-rw-r--r--. 1 root root 8262189056 Jun  6 00:19 test.log
[root@lvm ~]# umount /data-snap
[root@lvm ~]# ll /data-snap/
total 0
```
Откатимся на снепшот
```
[root@lvm ~]# umount /data
[root@lvm ~]# lvconvert --merge /dev/otus/test-snap
  Merging of volume otus/test-snap started.
  otus/test: Merged: 100.00%
[root@lvm ~]# mount /dev/otus/test /data
[root@lvm ~]# ll /data
total 8068564
drwx------. 2 root root      16384 Jun  6 00:15 lost+found
-rw-r--r--. 1 root root 8262189056 Jun  6 00:19 test.log
```
## LVM Mirroring
```
[root@lvm ~]# pvcreate /dev/sd{d,e}
  Physical volume "/dev/sdd" successfully created.
  Physical volume "/dev/sde" successfully created.
[root@lvm ~]# vgcreate vg0 /dev/sd{d,e}
  Volume group "vg0" successfully created
[root@lvm ~]# lvcreate -l+80%FREE -m1 -n mirror vg0
  Logical volume "mirror" created.
[root@lvm ~]# lvs
  LV       VG         Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  LogVol00 VolGroup00 -wi-ao---- <37.47g                                                    
  LogVol01 VolGroup00 -wi-ao----   1.50g                                                    
  small    otus       -wi-a----- 100.00m                                                    
  test     otus       -wi-ao----  10.00g                                                    
  mirror   vg0        rwi-a-r--- 816.00m                                    35.18 
  ```


# Домашнее задание 
Смотрим, что с в виртуальной машине с дисковыми устройствами 
```
[root@lvm ~]# lvmdiskscan
  /dev/VolGroup00/LogVol00 [     <37.47 GiB] 
  /dev/VolGroup00/LogVol01 [       1.50 GiB] 
  /dev/sda2                [       1.00 GiB] 
  /dev/sda3                [     <39.00 GiB] LVM physical volume
  /dev/sdb                 [      10.00 GiB] 
  /dev/sdc                 [       2.00 GiB] 
  /dev/sdd                 [       1.00 GiB] 
  /dev/sde                 [       1.00 GiB] 
  4 disks
  3 partitions
  0 LVM physical volume whole disks
  1 LVM physical volume
  ```
Готовим временный том для раздела 
```
root@lvm ~]# pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[root@lvm ~]# vgcreate vg_root /dev/sdb
  Volume group "vg_root" successfully created
[root@lvm ~]# lvcreate -n lv_root -l +100%FREE /dev/vg_root
  Logical volume "lv_root" created.
```
Создадим на нем файловую систему и смонтируем его, чтобы перенести туда данные:
```
[root@lvm ~]# mkfs.xfs /dev/vg_root/lv_root
meta-data=/dev/vg_root/lv_root   isize=512    agcount=4, agsize=655104 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0, sparse=0
data     =                       bsize=4096   blocks=2620416, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
[root@lvm ~]# mount /dev/vg_root/lv_root /mnt
```
скопируем все данные с / раздела в /mnt:
```
[root@lvm ~]# xfsdump -J - /dev/VolGroup00/LogVol00 | xfsrestore -J - /mnt
xfsdump: using file dump (drive_simple) strategy
xfsdump: version 3.1.7 (dump format 3.0)
xfsdump: level 0 dump of lvm:/
xfsdump: dump date: Sat Jun  6 00:51:58 2020
xfsdump: session id: 12eb5b3a-df72-4270-b77c-ef45651d22f2
xfsdump: session label: ""
xfsrestore: using file dump (drive_simple) strategy
xfsrestore: version 3.1.7 (dump format 3.0)
xfsrestore: searching media for dump
xfsdump: ino map phase 1: constructing initial dump list
xfsdump: ino map phase 2: skipping (no pruning necessary)
xfsdump: ino map phase 3: skipping (only one dump stream)
xfsdump: ino map construction complete
xfsdump: estimated dump size: 728232832 bytes
xfsdump: creating dump session media file 0 (media 0, file 0)
xfsdump: dumping ino map
xfsdump: dumping directories
xfsrestore: examining media file 0
xfsrestore: dump description: 
xfsrestore: hostname: lvm
xfsrestore: mount point: /
xfsrestore: volume: /dev/mapper/VolGroup00-LogVol00
xfsrestore: session time: Sat Jun  6 00:51:58 2020
xfsrestore: level: 0
xfsrestore: session label: ""
xfsrestore: media label: ""
xfsrestore: file system id: b60e9498-0baa-4d9f-90aa-069048217fee
xfsrestore: session id: 12eb5b3a-df72-4270-b77c-ef45651d22f2
xfsrestore: media id: e060622c-1f45-42bf-9b7f-8f1ac17f9792
xfsrestore: searching media for directory dump
xfsrestore: reading directories
xfsdump: dumping non-directory files
xfsrestore: 2727 directories and 23676 entries processed
xfsrestore: directory post-processing
xfsrestore: restoring non-directory files
xfsdump: ending media file
xfsdump: media file size 704971080 bytes
xfsdump: dump size (non-dir files) : 691768512 bytes
xfsdump: dump complete: 24 seconds elapsed
xfsdump: Dump Status: SUCCESS
xfsrestore: restore complete: 25 seconds elapsed
xfsrestore: Restore Status: SUCCESS
```
Проверяем, что все скопировалось
```
[root@lvm ~]# ls /mnt
bin   dev  home  lib64  mnt  proc  run   srv  tmp  vagrant
boot  etc  lib   media  opt  root  sbin  sys  usr  var
```
Затем переконфигурируем grub для того, чтобы при старте перейти в новый /
Сымитируем текущий root -> сделаем в него chroot и обновим grub:
```
[root@lvm ~]# for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done
[root@lvm ~]# chroot /mnt/
[root@lvm /]# grub2-mkconfig -o /boot/grub2/grub.cfg
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-3.10.0-862.2.3.el7.x86_64
Found initrd image: /boot/initramfs-3.10.0-862.2.3.el7.x86_64.img
done
```
Обновим образ `initrd` при помощи команды
```
[root@lvm /]# cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g;
> s/.img//g"` --force; done
```
Получаем выдачу 
```
Executing: /sbin/dracut -v initramfs-3.10.0-862.2.3.el7.x86_64.img 3.10.0-862.2.3.el7.x86_64 --force
dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
*** Including module: bash ***
*** Including module: nss-softokn ***
*** Including module: i18n ***
*** Including module: drm ***
*** Including module: plymouth ***
*** Including module: dm ***
Skipping udev rule: 64-device-mapper.rules
Skipping udev rule: 60-persistent-storage-dm.rules
Skipping udev rule: 55-dm.rules
*** Including module: kernel-modules ***
Omitting driver floppy
*** Including module: lvm ***
Skipping udev rule: 64-device-mapper.rules
Skipping udev rule: 56-lvm.rules
Skipping udev rule: 60-persistent-storage-lvm.rules
*** Including module: qemu ***
*** Including module: resume ***
*** Including module: rootfs-block ***
*** Including module: terminfo ***
*** Including module: udev-rules ***
Skipping udev rule: 40-redhat-cpu-hotplug.rules
Skipping udev rule: 91-permissions.rules
*** Including module: biosdevname ***
*** Including module: systemd ***
*** Including module: usrmount ***
*** Including module: base ***
*** Including module: fs-lib ***
*** Including module: shutdown ***
*** Including modules done ***
*** Installing kernel module dependencies and firmware ***
*** Installing kernel module dependencies and firmware done ***
*** Resolving executable dependencies ***
*** Resolving executable dependencies done***
*** Hardlinking files ***
*** Hardlinking files done ***
*** Stripping files ***
*** Stripping files done ***
*** Generating early-microcode cpio image contents ***
*** No early-microcode cpio image needed ***
*** Store current command line parameters ***
*** Creating image file ***
*** Creating image file done ***
*** Creating initramfs image file '/boot/initramfs-3.10.0-862.2.3.el7.x86_64.img' done ***
```
Для того, чтобы при загрузке был смонтирован нужный root нужно в файле
`/boot/grub2/grub.cfg` заменить `rd.lvm.lv=VolGroup00/LogVol00` на `rd.lvm.lv=vg_root/lv_root`
```
[root@lvm ~]# sed -i 's+rd.lvm.lv=VolGroup00/LogVol00+rd.lvm.lv=vg_root/lv_root+g' /boot/grub2/grub.cfg 
```
ПЕРЕЗАГРУЖАЕМСЯ
```
root@lvm ~]# reboot
Connection to 127.0.0.1 closed by remote host.
Connection to 127.0.0.1 closed.
[alexeyv@localhost stands-03-lvm]$ vagrant ssh
Last login: Sat Jun  6 00:49:48 2020 from 10.0.2.2
```
Перезились успешно с новым рут томом. Смотрим, что все получилось про помощи вывода lsblk:
```
[vagrant@lvm ~]$ lsblk
NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                       8:0    0   40G  0 disk 
├─sda1                    8:1    0    1M  0 part 
├─sda2                    8:2    0    1G  0 part /boot
└─sda3                    8:3    0   39G  0 part 
  ├─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
  └─VolGroup00-LogVol00 253:2    0 37.5G  0 lvm  
sdb                       8:16   0   10G  0 disk 
└─vg_root-lv_root       253:0    0   10G  0 lvm  /
sdc                       8:32   0    2G  0 disk 
sdd                       8:48   0    1G  0 disk 
sde                       8:64   0    1G  0 disk 
```

Теперь нам нужно изменить размер старой VG и вернуть на него рут. Длāя этого удаляем
старый LV размеров в 40G и создаем новый на 8G:
```
[root@lvm ~]# lvremove /dev/VolGroup00/LogVol00
Do you really want to remove active logical volume VolGroup00/LogVol00? [y/n]: y
  Logical volume "LogVol00" successfully removed
[root@lvm ~]# lvcreate -n VolGroup00/LogVol00 -L 8G /dev/VolGroup00
WARNING: xfs signature detected on /dev/VolGroup00/LogVol00 at offset 0. Wipe it? [y/n]: y
  Wiping xfs signature on /dev/VolGroup00/LogVol00.
  Logical volume "LogVol00" created.
```
Опять создадим на нем файловую систему и смонтируем его, чтобы перенести туда данные
```
[root@lvm ~]# mkfs.xfs /dev/VolGroup00/LogVol00
meta-data=/dev/VolGroup00/LogVol00 isize=512    agcount=4, agsize=524288 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0, sparse=0
data     =                       bsize=4096   blocks=2097152, imaxpct=25  
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
[root@lvm ~]# mount /dev/VolGroup00/LogVol00 /mnt
```
скопируем все данные с /lv_root в /mnt:
```
[root@lvm ~]# xfsdump -J - /dev/vg_root/lv_root | xfsrestore -J - /mnt
```
получаем выдачу 
```
xfsrestore: using file dump (drive_simple) strategy
xfsrestore: version 3.1.7 (dump format 3.0)
xfsdump: using file dump (drive_simple) strategy
xfsdump: version 3.1.7 (dump format 3.0)
xfsdump: level 0 dump of lvm:/
xfsdump: dump date: Sat Jun  6 01:20:08 2020
xfsdump: session id: 2fe9c311-f2dd-440d-84e1-2d207712b34e
xfsdump: session label: ""
xfsrestore: searching media for dump
xfsdump: ino map phase 1: constructing initial dump list
xfsdump: ino map phase 2: skipping (no pruning necessary)
xfsdump: ino map phase 3: skipping (only one dump stream)
xfsdump: ino map construction complete
xfsdump: estimated dump size: 1141989952 bytes
xfsdump: creating dump session media file 0 (media 0, file 0)
xfsdump: dumping ino map
xfsdump: dumping directories
xfsrestore: examining media file 0
xfsrestore: dump description: 
xfsrestore: hostname: lvm
xfsrestore: mount point: /
xfsrestore: volume: /dev/mapper/vg_root-lv_root
xfsrestore: session time: Sat Jun  6 01:20:08 2020
xfsrestore: level: 0
xfsrestore: session label: ""
xfsrestore: media label: ""
xfsrestore: file system id: 37af46f4-1b38-4ffd-96c7-508edb31ca6d
xfsrestore: session id: 2fe9c311-f2dd-440d-84e1-2d207712b34e
xfsrestore: media id: 5c66c2a1-c146-4b24-aa70-03ffab2efe3a
xfsrestore: searching media for directory dump
xfsrestore: reading directories
xfsdump: dumping non-directory files
xfsrestore: 4259 directories and 35316 entries processed
xfsrestore: directory post-processing
xfsrestore: restoring non-directory files
xfsdump: ending media file
xfsdump: media file size 1100318288 bytes
xfsdump: dump size (non-dir files) : 1079818144 bytes
xfsdump: dump complete: 44 seconds elapsed
xfsdump: Dump Status: SUCCESS
xfsrestore: restore complete: 44 seconds elapsed
xfsrestore: Restore Status: SUCCESS
```
Так же как в первый раз переконфигурируем grub, за исключением правки /etc/grub2/grub.cfg
```
[root@lvm ~]# for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done
[root@lvm ~]# chroot /mnt/
[root@lvm /]# grub2-mkconfig -o /boot/grub2/grub.cfg
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-3.10.0-862.2.3.el7.x86_64
Found initrd image: /boot/initramfs-3.10.0-862.2.3.el7.x86_64.img
done
```

```
[root@lvm /]# cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g;
> s/.img//g"` --force; done
Executing: /sbin/dracut -v initramfs-3.10.0-862.2.3.el7.x86_64.img 3.10.0-862.2.3.el7.x86_64 --force
dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
*** Including module: bash ***
*** Including module: nss-softokn ***
*** Including module: i18n ***
*** Including module: drm ***
*** Including module: plymouth ***
*** Including module: dm ***
Skipping udev rule: 64-device-mapper.rules
Skipping udev rule: 60-persistent-storage-dm.rules
Skipping udev rule: 55-dm.rules
*** Including module: kernel-modules ***
Omitting driver floppy
*** Including module: lvm ***
Skipping udev rule: 64-device-mapper.rules
Skipping udev rule: 56-lvm.rules
Skipping udev rule: 60-persistent-storage-lvm.rules
*** Including module: qemu ***
*** Including module: resume ***
*** Including module: rootfs-block ***
*** Including module: terminfo ***
*** Including module: udev-rules ***
Skipping udev rule: 40-redhat-cpu-hotplug.rules
Skipping udev rule: 91-permissions.rules
*** Including module: biosdevname ***
*** Including module: systemd ***
*** Including module: usrmount ***
*** Including module: base ***
*** Including module: fs-lib ***
*** Including module: shutdown ***
*** Including modules done ***
*** Installing kernel module dependencies and firmware ***
*** Installing kernel module dependencies and firmware done ***
*** Resolving executable dependencies ***
*** Resolving executable dependencies done***
*** Hardlinking files ***
*** Hardlinking files done ***
*** Stripping files ***
*** Stripping files done ***
*** Generating early-microcode cpio image contents ***
*** No early-microcode cpio image needed ***
*** Store current command line parameters ***
*** Creating image file ***
*** Creating image file done ***
*** Creating initramfs image file '/boot/initramfs-3.10.0-862.2.3.el7.x86_64.img' done ***
```
Пока не перезагружаемся и не выходим из под chroot - мы можем заодно перенести /var

Выделить том под /var в зеркало

На свободных дисках создаем зеркало:
```
root@lvm boot]# pvcreate /dev/sdc /dev/sdd
  Physical volume "/dev/sdc" successfully created.
  Physical volume "/dev/sdd" successfully created.
[root@lvm boot]# vgcreate vg_var /dev/sdc /dev/sdd
  Volume group "vg_var" successfully created
[root@lvm boot]# lvcreate -L 950M -m1 -n lv_var vg_var
  Rounding up size to full physical extent 952.00 MiB
  Logical volume "lv_var" created.
```

Создаем на нем ФС и перемещаем туда /var:

```
[root@lvm boot]# mkfs.ext4 /dev/vg_var/lv_var
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
60928 inodes, 243712 blocks
12185 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=249561088
8 block groups
32768 blocks per group, 32768 fragments per group
7616 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (4096 blocks): done
Writing superblocks and filesystem accounting information: done

[root@lvm boot]# mount /dev/vg_var/lv_var /mnt
[root@lvm boot]# cp -aR /var/* /mnt/ # rsync -avHPSAX /var/ /mnt/
```
На всякий случай сохраняем содержимое старого var (или же можно его просто удалить):
```
[root@lvm boot]# mkdir /tmp/oldvar && mv /var/* /tmp/oldvar
```
Монтируем новый var в каталог /var:
```
[root@lvm boot]# umount /mnt
[root@lvm boot]# mount /dev/vg_var/lv_var /var
```
Правим `fstab` для автоматического монтирования `/var`:
```
[root@lvm boot]# echo "`blkid | grep var: | awk '{print $2}'` /var ext4 defaults 0 0" >> /etc/fstab
```

Теперь успешно перезагружаемся в новый (уменьшнный root)
```
root@lvm ~]# exit
logout
[vagrant@lvm ~]$ reboot
==== AUTHENTICATING FOR org.freedesktop.login1.reboot ===
Authentication is required for rebooting the system.
Authenticating as: root
Password: 
==== AUTHENTICATION COMPLETE ===
[vagrant@lvm ~]$ Connection to 127.0.0.1 closed by remote host.
Connection to 127.0.0.1 closed.
[alexeyv@localhost stands-03-lvm]$ vagrant ssh
Last login: Sat Jun  6 01:17:35 2020 from 10.0.2.2
[vagrant@lvm ~]$ su - root
Password: 
Last login: Sat Jun  6 01:18:47 UTC 2020 on pts/0
```
Удаляем временную Volume Group:
```
[root@lvm ~]# lvremove /dev/vg_root/lv_root
Do you really want to remove active logical volume vg_root/lv_root? [y/n]: y
  Logical volume "lv_root" successfully removed
[root@lvm ~]# vgremove /dev/vg_root
  Volume group "vg_root" successfully removed
[root@lvm ~]# pvremove /dev/sdb
  Labels on physical volume "/dev/sdb" successfully wiped.
```
Выделяем том под /home по тому же принципу что делали для /var:
```
[root@lvm ~]# lvcreate -n LogVol_Home -L 2G /dev/VolGroup00
  Logical volume "LogVol_Home" created.
[root@lvm ~]# mkfs.xfs /dev/VolGroup00/LogVol_Home
meta-data=/dev/VolGroup00/LogVol_Home isize=512    agcount=4, agsize=131072 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0, sparse=0
data     =                       bsize=4096   blocks=524288, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
[root@lvm ~]# mount /dev/VolGroup00/LogVol_Home /mnt/
[root@lvm ~]# cp -aR /home/* /mnt/
[root@lvm ~]# rm -rf /home/*
[root@lvm ~]# umount /mnt
[root@lvm ~]# mount /dev/VolGroup00/LogVol_Home /home/
```
Правим fstab длā автоматического монтирования /home
```
[root@lvm ~]# echo "`blkid | grep Home | awk '{print $2}'` /home xfs defaults 0 0" >> /etc/fstab
```
Сгенерируем файлы в /home/:
```
[root@lvm ~]# touch /home/file{1..20}
[root@lvm ~]# ll /home/
total 0
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file1
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file10
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file11
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file12
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file13
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file14
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file15
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file16
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file17
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file18
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file19
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file2
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file20
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file3
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file4
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file5
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file6
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file7
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file8
-rw-r--r--. 1 root    root     0 Jun  6 01:39 file9
```
Снимаем Snapshot

```
[root@lvm ~]# lvcreate -L 100MB -s -n home_snap /dev/VolGroup00/LogVol_Home
  Rounding up size to full physical extent 128.00 MiB
  Logical volume "home_snap" created.
```

Удаляем часть файлов 
```
[root@lvm ~]# rm -f /home/file{11..20}
```
Теперь восстанавливаеся со снапшота 
```
[root@lvm ~]# umount /home
[root@lvm ~]# lvconvert --merge /dev/VolGroup00/home_snap
  Merging of volume VolGroup00/home_snap started.
  VolGroup00/LogVol_Home: Merged: 100.00%
[root@lvm ~]# mount /home
```
### Конец
